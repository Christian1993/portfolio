<?php

Route::get('/', 'PagesController@index');
Route::get('/projects', 'PagesController@projects');
Route::get('/skills', 'PagesController@skills');
Route::get('/about', 'PagesController@about');
Route::get('/workshop', function (){
    return view('pages.workshop');
});
