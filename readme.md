
# Portfolio

#### Description:
The project constists of four pages:
* Home - contains basic information about author and data required to contact
* Projects - here can be found the list of projects made by author
* Skills -  here can you check author's skills in the form of simple game
* About - contains downloable resume
There is also developed language changing  functionality
#### Technologies used:
* laravel, 
* javascript, 
* php,
* sass, 
* html, 
* bootstrap (only grid system), 

#### How to run:
1. Download repo
2. In main directory type: _npm_ _install_
3. Type: _npm_ _run_ _dev_

### Or visit live:
http://www.portfolio-webdevelopera.pl/home