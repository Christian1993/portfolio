<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .item{
            display:none;
        }
        .news, .sport, .other, .favorites{
            display: block;
        }
    </style>
    <title>
        @if(isset($title))
            {{$title}}
        @else
            {{config('app.name', 'My resume')}}
        @endif
    </title>

</head>
<body>
@include('includes.navbar')
    <div class="container">
        @yield('content')
    </div>
@include('includes.footer')
<script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>
</body>
</html>
