<nav class="navbar">
    <div class="container">
        <button class="hamburger-button" id="navbar-btn"></button>
        <ul class="navbar__list">
            <li class="navbar__list__item"><a class="link link--menu" href="/">Strona główna</a></li>
            <li class="navbar__list__item"><a class="link link--menu" href="/projects">Projekty</a></li>
            <li class="navbar__list__item"><a class="link link--menu" href="/skills">Umiejętności</a></li>
            <li class="navbar__list__item"><a class="link link--menu" href="/about">Życiorys</a></li>
        </ul>
    </div>
</nav>