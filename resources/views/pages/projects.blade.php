@extends('layouts.app')

@section('content')
    <h1>Wybrane projekty</h1>

    <section class="projects">
        <h2>We współpracy z zespołem</h2>
        <div class="projects__item">
            <img class="projects__item__photo" src="" alt="">
            <div class="projects__item__description">
                <p class="projects__item__description--title">Użyte technologie:</p>
                <p>TeamSite,JS, ES6, jQuery, sass, html, gulp, webpack</p>
                <p class="projects__item__description--title">Linki:</p>
                <a class="projects__item__description--link" href="">Strona live</a>
                <a class="projects__item__description--link" href="">Github</a>
            </div>
        </div>
    </section>

    <section class="projects">
        <h2>Samodzielne</h2>

    </section>
@endsection