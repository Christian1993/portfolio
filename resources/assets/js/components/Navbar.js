export default class Navbar {
  constructor(
    btnId = '#navbar-btn',
    btnActive = 'hamburger-button--active',
    navClass = '.navbar',
    navResponsiveClass = 'navbar--responsive'){

    this.btnId = btnId;
    this.btnActive = btnActive;
    this.navClass = navClass;
    this.navResponsiveClass = navResponsiveClass;
    this.init();
  }

  init(btnId = this.btnId, navClass = this.navClass){
    const btn = document.querySelector(btnId);
    const navbar = document.querySelector(navClass);
    if(btn && navbar){
      this.addEventListeners();
    }
  }
  addEventListeners(btnId= this.btnId){
    document.querySelector(btnId)
    .addEventListener('click', e => this.toggleNavbar(e))
  }
  toggleNavbar(e,navClass = this.navClass, navResponsiveClass = this.navResponsiveClass, btnActive= this.btnActive ){
    const navbar = document.querySelector(navClass);

    if(navbar.classList.contains(navResponsiveClass)){
      navbar.classList.remove(navResponsiveClass);
      e.target.classList.remove(btnActive);

    } else {
      navbar.classList.add(navResponsiveClass);
      e.target.classList.add(btnActive);
    }
  }
}