<?php

namespace App\Http\Controllers;

class PagesController extends Controller {
    public function index(){
        $title = 'Frontend developer cv';
        return view('pages.index')->with('title', $title);
    }
    public function projects(){
        $title = 'Projects';
        return view('pages.projects')->with('title', $title);
    }
    public function skills(){
        $title = 'Skills';
        return view('pages.skills')->with('title', $title);
    }
    public function about(){
        $title = 'About me';
        return view('pages.about')->with('title', $title);
    }
}
